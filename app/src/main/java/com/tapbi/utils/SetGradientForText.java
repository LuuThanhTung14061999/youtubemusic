package com.tapbi.utils;

import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.widget.TextView;

import static com.tapbi.common.Constant.colorEnd;
import static com.tapbi.common.Constant.colorStart;

public class SetGradientForText {
    public void setGradientTitleForText(TextView textView,String title,int icon){
        textView.setText(title);
        Shader textShader=new LinearGradient(0,0, textView.getWidth(),textView.getHeight(),colorStart, colorEnd,
                Shader.TileMode.REPEAT);
        textView.setCompoundDrawablesRelativeWithIntrinsicBounds(icon,0,0,0);
        textView.getPaint().setShader(textShader);
    }
}
