package com.tapbi.ui.base;

import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModelProvider;

public abstract class BaseActivity<BD extends ViewDataBinding, VM extends BaseViewModel> extends AppCompatActivity {

    protected BD mBinding;
    protected VM mViewModel;
    private PermissionCallback sPermissionCallback;

//    protected void showActionBar(ActionBar toolbar){
//        toolbar=getSupportActionBar();
////        ActionBar actionBar = getSupportActionBar();
////        actionBar.setDisplayHomeAsUpEnabled(true);
////        actionBar.setDisplayShowHomeEnabled(true);
//    }


    protected void checkPermission(String[] permission, PermissionCallback permissionCallback) {
        if (checkPermission(permission, true)) {
            permissionCallback.onGranted();
        } else {
            this.sPermissionCallback = permissionCallback;
        }
    }

    private boolean checkPermission(String[] permission, boolean withRequest) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (String p : permission) {
                if (checkSelfPermission(p) != PackageManager.PERMISSION_GRANTED) {
                    if (withRequest) {
                        requestPermissions(permission, 0);
                    }
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, getLayoutId());
        mViewModel = new ViewModelProvider(this).get(getViewModelClass());
    }

    protected abstract Class<VM> getViewModelClass();

    @LayoutRes
    protected abstract int getLayoutId();

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(checkPermission(permissions,false)){
            sPermissionCallback.onGranted();
        }else {
            sPermissionCallback.onDenied();
        }
    }
}

