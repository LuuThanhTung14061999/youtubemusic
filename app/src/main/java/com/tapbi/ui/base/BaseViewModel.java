package com.tapbi.ui.base;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class BaseViewModel extends ViewModel {

    private CompositeDisposable mDisposable = new CompositeDisposable();
    protected MutableLiveData<Boolean> mIsLoading=new MutableLiveData<>();//khi xử lí show dialog

    private MutableLiveData<Throwable> mError=new MutableLiveData<>();

    protected <T> void doAction(AsyncAction<T> action, MutableLiveData<T> result) {

        Observable<T> observable = Observable.create(emitter -> {
            try {
                T t = action.getAction();
                emitter.onNext(t);
            } catch (Exception ex) {

                emitter.onError(ex);
            } finally {
                emitter.onComplete();
            }
        });
        observable.observeOn(Schedulers.io());
        observable.subscribeOn(AndroidSchedulers.mainThread());
        doAction(observable,result);
    }

    protected <T> void doAction(Observable<T> observable, MutableLiveData result){
        mIsLoading.postValue(true);
        mError.postValue(null);
        Disposable disposable=observable.subscribe(
                value->{
                    result.postValue(value);
                },
                error->{

                },
                ()->{

                }
        );
        this.mDisposable.add(disposable);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mDisposable.clear();
    }

    public MutableLiveData<Boolean> getIsLoading() {
        return mIsLoading;
    }

    public MutableLiveData<Throwable> getError() {
        return mError;
    }

    public interface AsyncAction<T> {
        T getAction() throws Exception;
    }
}
