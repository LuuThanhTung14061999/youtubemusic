package com.tapbi.ui.base;

public interface PermissionCallback {
    void onGranted();
    void onDenied();
}
