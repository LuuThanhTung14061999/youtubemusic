package com.tapbi.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.tapbi.R;
import com.tapbi.databinding.ActivitySplashBinding;
import com.tapbi.notification.DailyNotification;
import com.tapbi.ui.home.HomeActivity;
import com.tapbi.utils.SetGradientForText;

import java.util.concurrent.TimeUnit;

public class SplashActivity extends AppCompatActivity {

    private ActivitySplashBinding binding;
    private PeriodicWorkRequest mPeriodicWorkRequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        SetGradientForText setGradientForText = new SetGradientForText();
        setGradientForText.setGradientTitleForText(binding.tvTitleSplash,getResources().getString(R.string.youtube_music_splash_activity),0);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                        .getBoolean("isfirstrun", true);
                if (isFirstRun){
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                            .edit()
                            .putBoolean("isfirstrun", false).apply();
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 1000);

    }

}
