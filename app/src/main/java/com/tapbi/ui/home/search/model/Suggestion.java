package com.tapbi.ui.home.search.model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.tapbi.ui.base.BaseModel;

@Entity(tableName = "Suggestion")
@SuppressLint("ParcelCreator")
public class Suggestion extends BaseModel implements SearchSuggestion, Parcelable {
    @PrimaryKey(autoGenerate = true)
    private int ID;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @ColumnInfo
    private String mName;

    public Suggestion(String mName) {
        this.mName = mName;
    }

    @Override
    public String getBody() {
        return mName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }
}
