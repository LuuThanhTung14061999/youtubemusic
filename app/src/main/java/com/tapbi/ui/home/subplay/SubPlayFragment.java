package com.tapbi.ui.home.subplay;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.tapbi.R;
import com.tapbi.data.local.model.video.Item;
import com.tapbi.data.local.model.video.SOAnswersResponse;
import com.tapbi.databinding.FragmentSubPlayBinding;
import com.tapbi.ui.base.BaseFragment;
import com.tapbi.ui.home.HomeActivity;
import com.tapbi.utils.RecyclerItemClickListener;
import com.tapbi.utils.ConvertCount;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class SubPlayFragment extends BaseFragment<FragmentSubPlayBinding, SubPlayViewModel> implements VideoSubPlayFragmentAdapter.DataVmd {

    private String query = "Music";
    private VideoSubPlayFragmentAdapter adapter;
    private List<Item> videos = new ArrayList<>();

    private ConvertCount convertCount = new ConvertCount();

    private boolean isCheckOnCLickItem=false;

    @Override
    protected Class<SubPlayViewModel> getViewModelClass() {
        return SubPlayViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_sub_play;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initAdapter();
        mViewModel = ViewModelProviders.of(this).get(SubPlayViewModel.class);

        mBinding.rcVideoPlayFragment.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), mBinding.rcVideoPlayFragment, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (position >= 0 && !isCheckOnCLickItem) {
                            isCheckOnCLickItem=true;
                            videos.remove(position);
                            adapter.setData(videos);
                            ((HomeActivity) getActivity()).showPlayScreenSubLayFragment(videos.get(position));
                            reloadData(videos.get(position));
                            isCheckOnCLickItem=false;
                        }
                    }
                }));

    }

    @Override
    public void onResume() {
        super.onResume();
        mViewModel.initPlayActivity(query);
        mViewModel.getVideosSubPlayFragment().observeForever(new Observer<SOAnswersResponse>() {
            @Override
            public void onChanged(SOAnswersResponse videoYoutube) {
                List<Item> items = videoYoutube.getItems();
                videos.addAll(items);
                adapter.notifyDataSetChanged();
            }
        });
    }

    public Item getItemFirst() {
        return videos.get(0);
    }

    public void updatePlayScreen() {
        reloadData(videos.get(0));
        videos.remove(videos.get(0));
        adapter.notifyDataSetChanged();
    }

    private void initAdapter() {
        if (adapter == null) {
            adapter = new VideoSubPlayFragmentAdapter(getLayoutInflater(), this);
            mBinding.rcVideoPlayFragment.setAdapter(adapter);
            adapter.setData(videos);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @SuppressLint("SetTextI18n")
    public void reloadData(Item video) {
        mBinding.title.setText(video.getSnippet().getTitle());
        mViewModel.getCommentYoutube(video, mBinding.imageUserComment, mBinding.contentOfComment);
        mViewModel.getChannelYoutube(video, mBinding.imChanel, mBinding.viewCountOfChannel);
        mBinding.titleChannel.setText(video.getSnippet().getChannelTitle());
        mViewModel.getViewCountById(video, mBinding.viewCount, mBinding.tvLike, mBinding.tvDislike, mBinding.numberOfComment, mBinding.cardviewImageuserComment, mBinding.contentOfComment);
        String time = convertCount.convertTime(video);
        mBinding.time.setText(time);
        String cmtCount = "";
        mBinding.numberOfComment.setText(cmtCount);
    }

    @Override
    public SubPlayViewModel getVmd() {
        return mViewModel;
    }

    public void setQuery(String q){
        query=q;
    }

}
