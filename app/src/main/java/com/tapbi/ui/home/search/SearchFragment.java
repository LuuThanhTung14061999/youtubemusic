package com.tapbi.ui.home.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.lifecycle.ViewModelProviders;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.SearchSuggestionsAdapter;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.tapbi.R;
import com.tapbi.data.local.database.room.AppDatabase;
import com.tapbi.data.local.model.video.Item;
import com.tapbi.data.local.model.video.SOAnswersResponse;
import com.tapbi.databinding.FragmentSearchBinding;
import com.tapbi.ui.base.BaseFragment;
import com.tapbi.ui.home.HomeActivity;
import com.tapbi.utils.RecyclerItemClickListener;
import com.tapbi.ui.home.search.adapter.TextSuggestionAdapter;
import com.tapbi.ui.home.search.adapter.VideoSearchAdapter;
import com.tapbi.ui.home.search.model.Suggestion;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.view.View.VISIBLE;

public class SearchFragment extends BaseFragment<FragmentSearchBinding, SearchViewModel> implements MakeSuggestion, TextSuggestionAdapter.TextSuggestionListener, FloatingSearchView.OnLeftMenuClickListener, FloatingSearchView.OnHomeActionClickListener, VideoSearchAdapter.DataVmd {

    private VideoSearchAdapter adapter;
    private List<Item> videos = new LinkedList<>();
    private List<Suggestion> mSuggestions = new LinkedList<>();
    private TextSuggestionAdapter textSuggestionAdapter;
    private boolean isCheckOnCLickItem = false;

    @Override
    protected Class<SearchViewModel> getViewModelClass() {
        return SearchViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_search;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((HomeActivity) getActivity()).setTitleScreen(mBinding.titleSearchFragment.tvTitle, getResources().getString(R.string.title_fragment_search), 0);
        hideListVideoSearch();
        textSuggestionAdapter = new TextSuggestionAdapter(getLayoutInflater());
        mBinding.rcText.setAdapter(textSuggestionAdapter);

        if (AppDatabase.getInMemoryDatabase(getActivity()).suggestionDao().getAll().size() > 0) {
            for (int i = AppDatabase.getInMemoryDatabase(getActivity()).suggestionDao().getAll().size() - 1; i >= 0; i--) {
                mSuggestions.add(AppDatabase.getInMemoryDatabase(getActivity()).suggestionDao().getAll().get(i));
            }
        }
        initData();
        textSuggestionAdapter.setListTextSuggestion(mSuggestions);
        textSuggestionAdapter.setTextSuggestionListener(this);
        mViewModel = ViewModelProviders.of(this).get(SearchViewModel.class);
        mBinding.search.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, String newQuery) {
                hideVoice();
                mBinding.recent.setVisibility(View.GONE);
                mBinding.rcText.setVisibility(View.GONE);
                if (newQuery.equals("")) {
                    showVoice();
                }
                mBinding.search.swapSuggestions(getSuggestion(newQuery));
            }
        });
        showTextSuggestion();
        mBinding.imVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                SearchFragment.this.startActivityForResult(intent, 0);
            }
        });
    }

    public void hideVoice() {
        mBinding.imVoice.setVisibility(View.GONE);
    }

    public void showVoice() {
        mBinding.imVoice.setVisibility(VISIBLE);
    }

    private void initData() {
        mSuggestions.add(new Suggestion("Ha Noi"));
        mSuggestions.add(new Suggestion("Anh"));
        mSuggestions.add(new Suggestion("Anh từng cố gắng"));
        mSuggestions.add(new Suggestion("Sai lầm của anh"));
        mSuggestions.add(new Suggestion("Yêu em nhưng không với tới"));
        mSuggestions.add(new Suggestion("Thằng hầu"));
        mSuggestions.add(new Suggestion("Tướng quân"));
        mSuggestions.add(new Suggestion("Nhạc trẻ remix"));
        mSuggestions.add(new Suggestion("Đấu la đại lục"));
        mSuggestions.add(new Suggestion("Tránh Duyên"));
    }

    private void hideListVideoSearch() {
        mBinding.recent.setVisibility(VISIBLE);
        mBinding.rcText.setVisibility(VISIBLE);
        mBinding.rcVideoSearch.setVisibility(View.GONE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == RESULT_OK) {
            ArrayList<String> results = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);
            mBinding.search.setSearchFocused(true);
            mBinding.search.setSearchText(results.get(0));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showTextSuggestion() {
        mBinding.search.setOnFocusChangeListener(new FloatingSearchView.OnFocusChangeListener() {
            @Override
            public void onFocus() {
                if (mBinding.search.getQuery().equals("")) {
                    showVoice();
                } else {
                    hideVoice();
                }
                mBinding.search.showProgress();
                mBinding.search.swapSuggestions(getSuggestion(mBinding.search.getQuery()));
                mBinding.search.hideProgress();
            }

            @Override
            public void onFocusCleared() {
                showVoice();
            }
        });
        mBinding.search.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {
                Suggestion suggestion = (Suggestion) searchSuggestion;
                mBinding.search.setSearchText(suggestion.getName());
                onSearchClickItem(suggestion.getName());
                mBinding.search.clearFocus();
            }

            @Override
            public void onSearchAction(String currentQuery) {
                boolean check = false;
                showVoice();
                showListVideoSearch();
                for (int i = 0; i < mSuggestions.size(); i++) {
                    if (mSuggestions.get(i).getName().toUpperCase().compareTo(currentQuery.toUpperCase()) == 0) {
                        check = true;
                        break;
                    }
                }
                if (!check) {
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            AppDatabase.getInMemoryDatabase(getContext()).suggestionDao().insertSuggestion(new Suggestion(currentQuery));
                        }
                    });
                    thread.start();
                    mSuggestions.add(0, new Suggestion(currentQuery));
                    textSuggestionAdapter.notifyDataSetChanged();
                }
                videos.clear();
                initializeAdapter(videos);
                mViewModel.initSearchView(currentQuery);
                mViewModel.getVideosRepositorySearchView().observe(getViewLifecycleOwner(), (SOAnswersResponse soAnswersResponse) -> {
                    List<Item> items = soAnswersResponse.getItems();
                    videos.addAll(items);
                    adapter.notifyDataSetChanged();
                });

            }
        });
        mBinding.search.setOnBindSuggestionCallback(new SearchSuggestionsAdapter.OnBindSuggestionCallback() {
            @Override
            public void onBindSuggestion(View suggestionView, ImageView leftIcon, TextView textView, SearchSuggestion item, int itemPosition) {
                leftIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                        R.drawable.icon_recent_search, null));
            }
        });
        mBinding.search.setOnMenuClickListener(this);
        mBinding.rcVideoSearch.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), mBinding.rcVideoSearch, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (position >= 0 && !isCheckOnCLickItem && !((HomeActivity) getActivity()).isCheckShowMaxPlayScreen())
                            isCheckOnCLickItem = true;
                        ((HomeActivity) getActivity()).showPlayScreen(videos.get(position));
                        isCheckOnCLickItem = false;
                    }
                })
        );
        mBinding.search.setOnHomeActionClickListener(this);
    }

    private void showListVideoSearch() {
        mBinding.rcText.setVisibility(View.GONE);
        mBinding.rcVideoSearch.setVisibility(VISIBLE);
        mBinding.recent.setVisibility(View.GONE);
    }

    private List<Suggestion> getSuggestion(String query) {
        List<Suggestion> suggestions = new ArrayList<>();
        for (Suggestion suggestion : mSuggestions) {
            if (suggestion.getName().toLowerCase().contains(query.toLowerCase())) {
                suggestions.add(suggestion);
            }
        }
        return suggestions;
    }

    private void initializeAdapter(List<Item> videos) {
        if (adapter == null) {
            adapter = new VideoSearchAdapter(getLayoutInflater(), this);
            mBinding.rcVideoSearch.setAdapter(adapter);
            adapter.setData(videos);
            mBinding.rcVideoSearch.setVisibility(VISIBLE);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void getSuggestion(List<Suggestion> suggestions) {
        mBinding.search.swapSuggestions(suggestions);
        mBinding.search.hideProgress();
    }

    @Override
    public void onTextSuggestionClicked(int position) {
        onSearchClickItem(mSuggestions.get(position).getName());
    }

    private void onSearchClickItem(String query) {
        mBinding.search.setSearchText(query);
        showVoice();
        showListVideoSearch();
        videos.clear();
        initializeAdapter(videos);
        mViewModel.initSearchView(query);
        mViewModel.getVideosRepositorySearchView().observe(getViewLifecycleOwner(), (SOAnswersResponse soAnswersResponse) -> {
            List<Item> items = soAnswersResponse.getItems();
            videos.addAll(items);
            adapter.notifyDataSetChanged();
        });
    }

    @Override
    public void onMenuOpened() {
    }

    @Override
    public void onMenuClosed() {
        showListVideoSearch();
    }

    @Override
    public void onHomeClicked() {
        hideListVideoSearch();
    }

    @Override
    public SearchViewModel getVmd() {
        return mViewModel;
    }

}
