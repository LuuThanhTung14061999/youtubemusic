package com.tapbi.ui.home.trending;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.tapbi.ui.base.BaseViewModel;
import com.tapbi.data.network.api.VideosRepository;
import com.tapbi.data.local.model.video.Item;
import com.tapbi.data.local.model.video.SOAnswersResponse;

import static com.tapbi.common.Constant.API_KEY;
import static com.tapbi.common.Constant.PART_SNIPPET;

public class TrendingViewModel extends BaseViewModel {
    private MutableLiveData<SOAnswersResponse> mutableLiveData;
    private VideosRepository videosRepositoryTrending;

    public void initTrending() {
        if (mutableLiveData != null) {
            return;
        }
        videosRepositoryTrending = VideosRepository.getInstance();
        mutableLiveData = videosRepositoryTrending.getVideosSearch(API_KEY, PART_SNIPPET, "Nhạc buồn chọn lọc 2020","VIDEO", 20);
    }
    public LiveData<SOAnswersResponse> getVideosRepositoryTrending() {
        return mutableLiveData;
    }

    public void getViewCountById(Item item, TextView textView, TextView textView1, ImageView imageView){
        videosRepositoryTrending.getChannel(item.getSnippet().getChannelId(),API_KEY,imageView);
        videosRepositoryTrending.getVideo(item.getId().getVideoId(),API_KEY,textView,textView1);
    }
}
