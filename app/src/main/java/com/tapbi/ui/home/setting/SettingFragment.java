package com.tapbi.ui.home.setting;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.tapbi.R;
import com.tapbi.databinding.FragmentSettingBinding;
import com.tapbi.notification.AlarmReceiver;
import com.tapbi.notification.DailyNotification;
import com.tapbi.ui.base.BaseFragment;
import com.tapbi.ui.home.HomeActivity;
import com.tapbi.ui.splash.SplashActivity;
import com.tapbi.utils.App;
import com.tapbi.utils.LocaleHelper;
import com.tapbi.utils.SetGradientForText;

import java.util.Calendar;

import static com.tapbi.common.Constant.REMINDER_WORK_NAME;
import static com.tapbi.common.Constant.colorEnd;
import static com.tapbi.common.Constant.colorStart;

public class SettingFragment extends BaseFragment<FragmentSettingBinding, SettingViewModel> {

    private SharedPreferences setPlayBackGround;
    private View view;
    private TextView tvItem1;
    private TextView tvItem2;
    private PopupWindow popup;
    String languagePref = "en";

    @Override
    protected Class<SettingViewModel> getViewModelClass() {
        return SettingViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_setting;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setTitleScreen(mBinding.titleSettingFragment.tvTitle, getResources().getString(R.string.youtube_music_splash_activity), R.drawable.icon_frame_menu);

        setPlayBackGround = getActivity().getSharedPreferences(
                "Play In Background", Context.MODE_PRIVATE);
        LayoutInflater inflater = (LayoutInflater)
                SettingFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        view = inflater.inflate(R.layout.popup_layout, null);
        tvItem1 = (TextView) view.findViewById(R.id.tv_1);
        tvItem2 = (TextView) view.findViewById(R.id.tv_2);

        changeLanguage();
        setPlayInBackGround();
        mBinding.policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                startActivity(browserIntent);
            }
        });
        showNotificationDaily();
    }

    private void changeLanguage() {
        popup = new PopupWindow(view, 300, LinearLayout.LayoutParams.WRAP_CONTENT, true);
        popup.setWidth(500);
        mBinding.language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvItem1.setText("English");
                tvItem2.setText("Vietnamese");
                if (setPlayBackGround.getString("Language", "1").compareTo("English") == 0) {
                    tvItem1.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic__done_24, 0);
                    tvItem2.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    tvItem2.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic__done_24, 0);
                    tvItem1.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    popup.showAsDropDown(v, 0, 0, Gravity.RIGHT);
                }
                SharedPreferences.Editor editor = setPlayBackGround.edit();
                tvItem1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        languagePref = "en";
                        editor.putString("Language", "English");
                        editor.apply();
                        tvItem1.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic__done_24, 0);
                        tvItem2.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
                        changeLangueAtRunTime();
                    }
                });
                tvItem2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        languagePref = "vi";
                        editor.putString("Language", "Vietnam");
                        editor.apply();
                        tvItem2.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic__done_24, 0);
                        tvItem1.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
                        changeLangueAtRunTime();
                    }
                });

            }
        });
    }

    private void changeLangueAtRunTime() {
        if (!languagePref.isEmpty()) {
            LocaleHelper.setLocale(getContext(), languagePref);
            Intent intent = new Intent(getActivity(), SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            getActivity().finish();
        }
    }

    private void setPlayInBackGround() {
        mBinding.playBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvItem1.setText("On");
                tvItem2.setText("Off");
                if (setPlayBackGround.getString("Play", "1").compareTo("Turn On") == 0) {
                    tvItem1.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic__done_24, 0);
                    tvItem2.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    tvItem2.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic__done_24, 0);
                    tvItem1.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    popup.showAsDropDown(v, 0, 0, Gravity.RIGHT);
                }
                SharedPreferences.Editor editor = setPlayBackGround.edit();
                tvItem1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editor.putString("Play", "Turn On");
                        editor.apply();
                        tvItem1.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic__done_24, 0);
                        tvItem2.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
                    }
                });
                tvItem2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editor.putString("Play", "Turn Off");
                        editor.apply();
                        tvItem2.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic__done_24, 0);
                        tvItem1.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);

                    }
                });
            }
        });
    }

    private void showNotificationDaily() {

        mBinding.getNotificationMorning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvItem1.setText("On");
                tvItem2.setText("Off");
                if (setPlayBackGround.getString("Play1", "1").compareTo("Turn On Notify") == 0) {
                    tvItem1.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic__done_24, 0);
                    tvItem2.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    tvItem2.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic__done_24, 0);
                    tvItem1.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    popup.showAsDropDown(v, 0, 0, Gravity.RIGHT);
                }
                SharedPreferences.Editor editor = setPlayBackGround.edit();
                tvItem1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editor.putString("Play1", "Turn On Notify");
                        editor.apply();
                        tvItem1.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic__done_24, 0);
                        tvItem2.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                            WorkManager workManager = WorkManager.getInstance();
                            OneTimeWorkRequest workRequestnew = new OneTimeWorkRequest.Builder(DailyNotification.class)
                                    .build();
                            workManager.enqueueUniqueWork(REMINDER_WORK_NAME, ExistingWorkPolicy.REPLACE, workRequestnew);
                        } else {
                            startAlarm(true, true);
                        }
                    }
                });
                tvItem2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editor.putString("Play1", "Turn Off Notify");
                        editor.apply();
                        tvItem2.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic__done_24, 0);
                        tvItem1.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
                        WorkManager.getInstance().cancelUniqueWork(REMINDER_WORK_NAME);
                    }
                });
            }
        });
    }

    private void startAlarm(boolean isNotification, boolean isRepeat) {
        AlarmManager manager = (AlarmManager) App.getContext().getSystemService(Context.ALARM_SERVICE);
        Intent myIntent;
        PendingIntent pendingIntent;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 7);
        calendar.set(Calendar.MINUTE, 1);
        calendar.set(Calendar.SECOND, 1);

        myIntent = new Intent((HomeActivity) getActivity(), AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast((HomeActivity) getActivity(), 0, myIntent, 0);

        if (!isRepeat)
            manager.set(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime() + 3000, pendingIntent);
        else {
            manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }

}
