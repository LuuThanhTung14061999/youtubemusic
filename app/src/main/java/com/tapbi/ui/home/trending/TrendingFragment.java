package com.tapbi.ui.home.trending;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.tapbi.R;
import com.tapbi.ui.base.BaseFragment;
import com.tapbi.data.local.model.video.Item;
import com.tapbi.data.local.model.video.SOAnswersResponse;
import com.tapbi.databinding.FragmentTrendingBinding;
import com.tapbi.ui.home.HomeActivity;
import com.tapbi.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class TrendingFragment extends BaseFragment<FragmentTrendingBinding, TrendingViewModel> implements VideoTrendingFragmentAdapter.DataVmd {

    private VideoTrendingFragmentAdapter adapter;
    private List<Item> videos = new ArrayList<>();
    private boolean isCheckOnCLickItem = false;

    @Override
    protected Class<TrendingViewModel> getViewModelClass() {
        return TrendingViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_trending;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setTitleScreen(mBinding.titleTrendingFragment.tvTitle
                , getResources().getString(R.string.title_trending), 0);
        mViewModel = ViewModelProviders.of(this).get(TrendingViewModel.class);
        mViewModel.initTrending();

        mViewModel.getVideosRepositoryTrending().observeForever(new Observer<SOAnswersResponse>() {
            @Override
            public void onChanged(SOAnswersResponse videoYoutube) {
                List<Item> items = videoYoutube.getItems();
                videos.addAll(items);
                adapter.notifyDataSetChanged();
            }
        });
        initializeAdapter(videos);
        mBinding.rcVideoHomeFragment.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), mBinding.rcVideoHomeFragment, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (position >= 0 && !isCheckOnCLickItem && !((HomeActivity) getActivity()).isCheckShowMaxPlayScreen())
                            isCheckOnCLickItem = true;
                        ((HomeActivity) getActivity()).showPlayScreen(videos.get(position));
                        isCheckOnCLickItem = false;
                    }
                })
        );

    }

    private void initializeAdapter(List<Item> videos) {
        if (adapter == null) {
            adapter = new VideoTrendingFragmentAdapter(getLayoutInflater(), this);
            mBinding.rcVideoHomeFragment.setAdapter(adapter);
            adapter.setData(videos);
        } else {
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public TrendingViewModel getVmd() {
        return mViewModel;
    }
}
