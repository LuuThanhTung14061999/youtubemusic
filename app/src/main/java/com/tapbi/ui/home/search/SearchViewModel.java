package com.tapbi.ui.home.search;

import android.widget.TextView;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.tapbi.ui.base.BaseViewModel;
import com.tapbi.data.network.api.VideosRepository;
import com.tapbi.data.local.model.video.Item;
import com.tapbi.data.local.model.video.SOAnswersResponse;

import static com.tapbi.common.Constant.API_KEY;
import static com.tapbi.common.Constant.PART_SNIPPET;

public class SearchViewModel extends BaseViewModel {
    private MutableLiveData<SOAnswersResponse> mutableLiveData;
    private VideosRepository videosRepositorySearchView;
    public void initSearchView(String query){
        videosRepositorySearchView = VideosRepository.getInstance();
        mutableLiveData = videosRepositorySearchView.getVideosSearch(API_KEY, PART_SNIPPET, query, "VIDEO", 50);

    }
    public LiveData<SOAnswersResponse> getVideosRepositorySearchView() {
        return mutableLiveData;
    }

    public void getViewCountById(Item item, TextView textView,TextView textView1){
        videosRepositorySearchView.getVideo(item.getId().getVideoId(),API_KEY,textView,textView1);
    }


}
