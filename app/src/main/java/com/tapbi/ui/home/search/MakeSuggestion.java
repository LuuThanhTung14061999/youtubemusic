package com.tapbi.ui.home.search;

import com.tapbi.ui.home.search.model.Suggestion;

import java.util.List;

public interface MakeSuggestion {
    void getSuggestion (List<Suggestion> suggestions) ;
}
