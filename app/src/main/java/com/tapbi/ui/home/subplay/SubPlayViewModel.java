package com.tapbi.ui.home.subplay;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.tapbi.ui.base.BaseViewModel;
import com.tapbi.data.network.api.VideosRepository;
import com.tapbi.data.local.model.video.Item;
import com.tapbi.data.local.model.video.SOAnswersResponse;

import static com.tapbi.common.Constant.API_KEY;
import static com.tapbi.common.Constant.PART_SNIPPET;

public class SubPlayViewModel extends BaseViewModel {
    private MutableLiveData<SOAnswersResponse> mutableLiveData;
    private VideosRepository videosRepositorySubPlayActivity;

    public void initPlayActivity(String query) {
        if (mutableLiveData != null) {
            return;
        }
        videosRepositorySubPlayActivity = VideosRepository.getInstance();
        mutableLiveData = videosRepositorySubPlayActivity.getVideosSearch(API_KEY, PART_SNIPPET, query, "VIDEO", 100);
    }

    public LiveData<SOAnswersResponse> getVideosSubPlayFragment() {
        return mutableLiveData;
    }

    public void getViewCountById(Item item, TextView textView1, TextView textView2, TextView textView3, TextView textView4, CardView cardView,TextView textView5) {
        videosRepositorySubPlayActivity.getVideo(item.getId().getVideoId(), API_KEY, textView1, textView2, textView3, textView4, cardView,textView5);
    }

    public void getViewCountById(Item item, TextView textView, TextView textView1, ImageView imageView) {
        videosRepositorySubPlayActivity.getChannel(item.getSnippet().getChannelId(), API_KEY, imageView);
        videosRepositorySubPlayActivity.getVideo(item.getId().getVideoId(), API_KEY, textView, textView1);
    }

    public void getCommentYoutube(Item video, ImageView imageView, TextView textView) {
        videosRepositorySubPlayActivity.getComment(5, video.getId().getVideoId(), API_KEY, imageView, textView);
    }

    public void getChannelYoutube(Item video, ImageView imageView, TextView textView) {
        videosRepositorySubPlayActivity.getChannel(video.getSnippet().getChannelId(), API_KEY, imageView, textView);
    }


}
