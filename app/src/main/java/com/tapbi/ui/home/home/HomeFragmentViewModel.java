package com.tapbi.ui.home.home;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.tapbi.ui.base.BaseViewModel;
import com.tapbi.data.network.api.VideosRepository;
import com.tapbi.data.local.model.video.Item;
import com.tapbi.data.local.model.video.SOAnswersResponse;

import static com.tapbi.common.Constant.API_KEY;
import static com.tapbi.common.Constant.PART_SNIPPET;

public class HomeFragmentViewModel extends BaseViewModel {

    private MutableLiveData<SOAnswersResponse> mutableLiveData;
    private VideosRepository videosRepositoryHomeFragment;

    public void initHomeFragment(){
        if (mutableLiveData != null){
            return;
        }
        videosRepositoryHomeFragment = VideosRepository.getInstance();
        mutableLiveData = videosRepositoryHomeFragment.getVideosSearch(API_KEY,PART_SNIPPET, "Nhạc buồn", "VIDEO", 15);

    }
    public LiveData<SOAnswersResponse> getNewsRepositoryHomeFragment() {
        return mutableLiveData;
    }

    public void getViewCountById(Item item, TextView textView, TextView textView1, ImageView imageView){
        videosRepositoryHomeFragment.getChannel(item.getSnippet().getChannelId(),API_KEY,imageView);
        videosRepositoryHomeFragment.getVideo(item.getId().getVideoId(),API_KEY,textView,textView1);
    }


}
