package com.tapbi.ui.home.search.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tapbi.databinding.ItemTextSuggestionBinding;
import com.tapbi.ui.home.search.model.Suggestion;

import java.util.List;

public class TextSuggestionAdapter extends RecyclerView.Adapter<TextSuggestionAdapter.TextSuggestionHolder> {

    private LayoutInflater layoutInflater;
    private TextSuggestionListener textSuggestionListener;
    private List<Suggestion> listTextSuggestion;

    public TextSuggestionAdapter(LayoutInflater layoutInflater) {
        this.layoutInflater = layoutInflater;
    }

    public TextSuggestionListener getTextSuggestionListener() {
        return textSuggestionListener;
    }

    public void setTextSuggestionListener(TextSuggestionListener textSuggestionListener) {
        this.textSuggestionListener = textSuggestionListener;
    }

    public List<Suggestion> getListTextSuggestion() {
        return listTextSuggestion;
    }

    public void setListTextSuggestion(List<Suggestion> listTextSuggestion) {
        this.listTextSuggestion = listTextSuggestion;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TextSuggestionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTextSuggestionBinding binding = ItemTextSuggestionBinding.inflate(layoutInflater, parent, false);
        return new TextSuggestionHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull TextSuggestionHolder holder, int position) {
        holder.binding.setTextSuggestion(listTextSuggestion.get(position));
        if(textSuggestionListener!=null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    textSuggestionListener.onTextSuggestionClicked(position);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return listTextSuggestion == null ? 0 : listTextSuggestion.size();
    }

    public class TextSuggestionHolder extends RecyclerView.ViewHolder {
        private ItemTextSuggestionBinding binding;

        public TextSuggestionHolder(@NonNull ItemTextSuggestionBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface TextSuggestionListener {
        void onTextSuggestionClicked(int position);
    }
}
