package com.tapbi.ui.home.home;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.tapbi.data.local.model.video.Item;
import com.tapbi.databinding.ItemVideoBinding;
import com.tapbi.utils.ConvertCount;

import java.util.List;

public class VideoHomeFragmentAdapter extends RecyclerView.Adapter<VideoHomeFragmentAdapter.VideoHolder> {

    public interface DataVmd {
        HomeFragmentViewModel getVmd();
    }

    private DataVmd dataVmd;
    private LayoutInflater layoutInflater;

    public VideoHomeFragmentAdapter(LayoutInflater layoutInflater, DataVmd dataVmd) {
        this.dataVmd = dataVmd;
        this.layoutInflater = layoutInflater;

    }

    private List<Item> data;

    public List<Item> getData() {
        return data;
    }

    public void setData(List<Item> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public VideoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemVideoBinding binding = ItemVideoBinding.inflate(layoutInflater, parent, false);
        return new VideoHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class VideoHolder extends RecyclerView.ViewHolder {
        private ItemVideoBinding binding;

        public VideoHolder(@NonNull ItemVideoBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @SuppressLint("SetTextI18n")
        public void bind(Item itemsBean) {
            dataVmd.getVmd().getViewCountById(itemsBean,binding.viewCount,binding.tvDuration,binding.imChanel);
            getChannelAndTime(itemsBean);
            binding.title.setText(itemsBean.getSnippet().getTitle());
            Glide.with(binding.imageView).load(itemsBean.getSnippet().getThumbnails().getHigh().getUrl()).centerCrop().into(binding.imageView);
        }

        @SuppressLint("SetTextI18n")
        private void getChannelAndTime(Item itemsBean) {
            ConvertCount convertCount = new ConvertCount();
            String time = convertCount.convertTime(itemsBean);
            binding.channel.setText(itemsBean.getSnippet().getChannelTitle() + " - ");
            binding.time.setText(time);
        }
    }

}
