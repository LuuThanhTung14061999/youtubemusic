package com.tapbi.ui.home.videoplay;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.RemoteViews;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.tapbi.R;
import com.tapbi.data.local.model.video.Item;
import com.tapbi.databinding.FragmentTopBinding;
import com.tapbi.ui.base.BaseFragment;
import com.tapbi.ui.home.HomeActivity;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static android.app.Notification.DEFAULT_ALL;
import static android.content.Context.NOTIFICATION_SERVICE;

public class VideoPlayFragment extends BaseFragment<FragmentTopBinding, VideoPlayViewModel> {
    private NotificationManager mNotificationManager;
    private String titleVideo = "";
    private String channelVideo = "";
    private String image = null;

    @Override
    public void onPause() {
        SharedPreferences setPlayBackGround = getActivity().getSharedPreferences(
                "Play In Background", Context.MODE_PRIVATE);
        if (setPlayBackGround != null) {
            if (!titleVideo.equals("")
                    && setPlayBackGround.getString("Play", "1").compareTo("Turn On") == 0) {
                mBinding.youtubeView.enableBackgroundPlayback(true);
                pushNotify();
                super.onPause();
            } else if (!titleVideo.equals("")
                    && setPlayBackGround.getString("Play", "2").compareTo("Turn Off") == 0) {
                mBinding.youtubeView.enableBackgroundPlayback(false);
                super.onPause();
            } else {
                super.onPause();
            }
        } else {
            super.onPause();
        }
    }

    public void closedNotification() {
        mNotificationManager.cancel(2);
    }

    private void pushNotify() {
        String channelId = "Youtube Channel";
        RemoteViews remoteViews = new RemoteViews(Objects.requireNonNull(getContext()).getPackageName(), R.layout.notification_custom);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            URL url = new URL(image);
            remoteViews.setImageViewBitmap(R.id.im_notication_custom, BitmapFactory.decodeStream((InputStream) url.getContent()));
        } catch (IOException e) {
        }
        remoteViews.setTextViewText(R.id.title_video_notification, titleVideo);
        remoteViews.setTextViewText(R.id.channel_video_notification, channelVideo);
        eventClickNotificationCustom(remoteViews, "Closed", "1", 0, R.id.im_closed_notifi);
        eventClickNotificationCustom(remoteViews, "Next", "2", 1, R.id.im_next);
        eventClickNotificationCustom(remoteViews, "Previous", "3", 2, R.id.im_previous);
        eventClickNotificationCustom(remoteViews, "Pause", "4", 3, R.id.im_pause);
        eventClickNotificationCustom(remoteViews, "Play", "5", 4, R.id.im_play);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                getContext(),
                channelId
        );

        builder.setDefaults(DEFAULT_ALL);
        builder.setStyle(new NotificationCompat.DecoratedCustomViewStyle());
        builder.setWhen(System.currentTimeMillis());
        builder.setSmallIcon(R.drawable.icon_frame_menu);
        builder.setCustomContentView(remoteViews);
        builder.setPriority(Notification.PRIORITY_MAX);
        mNotificationManager =
                (NotificationManager) getContext().getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "youtube notification",
                    NotificationManager.IMPORTANCE_HIGH);
            mNotificationManager.createNotificationChannel(channel);
            builder.setChannelId(channelId);
        }
        mNotificationManager.notify(2, builder.build());
    }

    private void eventClickNotificationCustom(RemoteViews remoteViews, String closed, String s, int i, int p) {
        Intent closeButton = new Intent(getActivity(), HomeActivity.class);
        closeButton.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        closeButton.putExtra(closed, s);
        PendingIntent pendingSwitchIntent = PendingIntent.getActivity(getContext(), i, closeButton, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(p, pendingSwitchIntent);
    }

    @Override
    public void onDestroy() {
        SharedPreferences setPlayBackGround = getActivity().getSharedPreferences(
                "Play In Background", Context.MODE_PRIVATE);
        if (setPlayBackGround != null) {
            String playBackGround = setPlayBackGround.getString("Turn On", "Turn On");
            if (!titleVideo.equals("") && playBackGround.compareTo("Turn On") == 0) {
                super.onDestroy();
                closedNotification();
            } else {
                super.onDestroy();
            }
        } else {
            super.onDestroy();
        }
    }

    public YouTubePlayer youTubePlayer;
    private List<Item> listVideoPrevious = new ArrayList<>();
    public String videoId = null;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.youtubeView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NotNull YouTubePlayer youTubePlayerQ) {
                youTubePlayer = youTubePlayerQ;
            }
        });
    }

    @Override
    protected Class<VideoPlayViewModel> getViewModelClass() {
        return VideoPlayViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_top;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public void playVideo(Item video) {
        channelVideo = video.getSnippet().getChannelTitle();
        titleVideo = video.getSnippet().getTitle();
        image = String.valueOf(video.getSnippet().getThumbnails().getHigh().getUrl());
        videoId = video.getId().getVideoId();
        ((HomeActivity) getActivity()).setChannelVideo(video.getSnippet().getChannelTitle());
        ((HomeActivity) getActivity()).setTitleVideo(video.getSnippet().getTitle());
        listVideoPrevious.add(video);
        youTubePlayer.loadVideo(videoId, 0);
        mBinding.youtubeView.getPlayerUiController().setCustomAction1(getResources().getDrawable(R.drawable.mdi_skip_previous), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backPressVideo();
            }
        });
        mBinding.youtubeView.getPlayerUiController().setCustomAction2(getResources().getDrawable(R.drawable.mdi_skip_next), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextVideo();
            }
        });
        mBinding.youtubeView.getPlayerUiController().showCustomAction1(true);
        mBinding.youtubeView.getPlayerUiController().showCustomAction2(true);
    }

    public void backPressVideo() {
        if (listVideoPrevious.size() >= 2) {
            String videoId = listVideoPrevious.get(listVideoPrevious.size() - 1).getId().getVideoId();
            ((HomeActivity) getActivity()).reloadDataFromSubPlayFragment(listVideoPrevious.get(listVideoPrevious.size() - 1));
            youTubePlayer.loadVideo(videoId, 0);
            listVideoPrevious.remove(listVideoPrevious.size() - 1);
        }
    }

    public void nextVideo() {
        Item item = ((HomeActivity) getActivity()).getItemFirst();
        youTubePlayer.loadVideo(item.getId().getVideoId(), 0);
        ((HomeActivity) getActivity()).updatePlayScreen();
        listVideoPrevious.add(item);
    }

    public void exitBackgroundVideo() {
        pauseVideo();
        mBinding.youtubeView.enableBackgroundPlayback(false);
    }

    public void pauseVideo() {
        youTubePlayer.pause();
    }

    public void playVideo() {
        youTubePlayer.play();
    }
}
