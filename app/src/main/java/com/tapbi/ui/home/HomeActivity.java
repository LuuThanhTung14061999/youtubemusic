package com.tapbi.ui.home;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.tapbi.R;
import com.tapbi.data.local.model.video.Item;
import com.tapbi.databinding.ActivityHomeBinding;
import com.tapbi.ui.base.BaseActivity;
import com.tapbi.ui.home.subplay.SubPlayFragment;
import com.tapbi.ui.home.videoplay.VideoPlayFragment;
import com.tapbi.ui.home.home.HomeFragment;
import com.tapbi.ui.home.search.SearchFragment;
import com.tapbi.ui.home.setting.SettingFragment;
import com.tapbi.ui.home.trending.TrendingFragment;
import com.tapbi.utils.SetGradientForText;
import com.tuanhav95.drag.DragView;

import java.util.LinkedList;
import java.util.List;

import static com.tapbi.common.Constant.colorBlack;
import static com.tapbi.common.Constant.colorEnd;
import static com.tapbi.common.Constant.colorStart;

public class HomeActivity extends BaseActivity<ActivityHomeBinding, HomeViewModel> {

    VideoPlayFragment videoPlayFragment = new VideoPlayFragment();
    SubPlayFragment subPlayFragment = new SubPlayFragment();
    private HomeFragment homeFragment = new HomeFragment();
    private TrendingFragment trendingFragment = new TrendingFragment();
    private SettingFragment settingFragment = new SettingFragment();
    private SearchFragment searchFragment = new SearchFragment();
    private String titleVideo;
    private String channelVideo;
    private boolean checkShowMaxPlayScreen = false;

    private boolean is1 = false;
    private boolean is2 = false;
    private boolean is3 = false;
    private boolean is4 = false;
    private boolean back = true;

    final FragmentManager fm = getSupportFragmentManager();
    Fragment active = homeFragment;

    private List<Integer> data = new LinkedList<>();

    private Item item = new Item();

    public boolean isCheckShowMaxPlayScreen() {
        return checkShowMaxPlayScreen;
    }

    public void setCheckShowMaxPlayScreen(boolean checkShowMaxPlayScreen) {
        this.checkShowMaxPlayScreen = checkShowMaxPlayScreen;
    }

    public List<Integer> getData() {
        return data;
    }

    public void setData(List<Integer> data) {
        this.data = data;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getExtras() != null && intent.getExtras().getString("Closed") != null &&
                intent.getExtras().getString("Closed").equals("1")) {
            videoPlayFragment.closedNotification();
            finish();
        } else if (intent.getExtras() != null && intent.getExtras().getString("Next") != null &&
                intent.getExtras().getString("Next").equals("2")) {
            videoPlayFragment.nextVideo();
            videoPlayFragment.closedNotification();
        } else if (intent.getExtras() != null && intent.getExtras().getString("Previous") != null &&
                intent.getExtras().getString("Previous").equals("3")) {
            videoPlayFragment.closedNotification();
            videoPlayFragment.backPressVideo();
        } else if (intent.getExtras() != null && intent.getExtras().getString("Pause") != null &&
                intent.getExtras().getString("Pause").equals("4")) {
            videoPlayFragment.pauseVideo();
            videoPlayFragment.closedNotification();
        }

    }

    public String getTitleVideo() {
        return titleVideo;
    }

    public void setTitleVideo(String titleVideo) {
        this.titleVideo = titleVideo;
    }

    public String getChannelVideo() {
        return channelVideo;
    }

    public void setChannelVideo(String channelVideo) {
        this.channelVideo = channelVideo;
    }

    @Override
    public void finishActivity(int requestCode) {
        super.finishActivity(requestCode);
    }

    @Override
    protected Class<HomeViewModel> getViewModelClass() {
        return HomeViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Shader textShader = new LinearGradient(0, 0, mBinding.tvHome.getWidth(), mBinding.tvHome.getHeight(), colorStart, colorEnd,
                Shader.TileMode.REPEAT);
        mBinding.tvHome.getPaint().setShader(textShader);
        fm.beginTransaction().add(R.id.frameTop, videoPlayFragment).commit();
        fm.beginTransaction().add(R.id.frameBottom, subPlayFragment).commit();
        mBinding.dragView.close();
        if (!is1) {
            is1 = true;
            fm.beginTransaction().add(R.id.frame_container, active, "1").addToBackStack(null).commit();
            data.add(1);
        }
        if (!is1 && !is2 && !is3 && !is4) {
            hideNavigation();
        }
        initListenerBottomNavigation();
    }

    private void initListenerBottomNavigation() {
        mBinding.tvHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickedHome();
            }
        });

        mBinding.tvSearch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                clickedSearch();
            }
        });

        mBinding.tvSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickedSetting();
            }
        });

        mBinding.tvTrending.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                clickedTrending();
            }
        });
    }

    private void clickedTrending() {
        setBottomNavigationNormal(mBinding.tvHome, R.drawable.ic_home_normal);
        setBottomNavigationSelected(mBinding.tvTrending, R.drawable.ic_trending_selected);
        setBottomNavigationNormal(mBinding.tvSearch, R.drawable.ic_search_normal);
        setBottomNavigationNormal(mBinding.tvSetting, R.drawable.ic_setting_normal);
        if (!is2) {
            is2 = true;
            fm.beginTransaction().add(R.id.frame_container, trendingFragment, "2").addToBackStack(null).hide(trendingFragment).commit();
        }
        fm.beginTransaction().hide(active).show(trendingFragment).commit();

        active = trendingFragment;
        if (back) {
            checkIsReady(2);
            data.add(2);
        } else {
            back = true;
        }
    }

    private void clickedSetting() {
        setBottomNavigationNormal(mBinding.tvHome, R.drawable.ic_home_normal);
        setBottomNavigationNormal(mBinding.tvTrending, R.drawable.ic_trending_normal);
        setBottomNavigationNormal(mBinding.tvSearch, R.drawable.ic_search_normal);
        setBottomNavigationSelected(mBinding.tvSetting, R.drawable.iv_setting_selected);
        if (!is4) {
            is4 = true;
            fm.beginTransaction().add(R.id.frame_container, settingFragment, "3").addToBackStack(null).hide(settingFragment).commit();
        }
        fm.beginTransaction().hide(active).show(settingFragment).commit();
        active = settingFragment;
        if (back) {
            checkIsReady(4);
            data.add(4);
        } else {
            back = true;
        }
    }

    private void clickedSearch() {
        setBottomNavigationNormal(mBinding.tvHome, R.drawable.ic_home_normal);
        setBottomNavigationNormal(mBinding.tvTrending, R.drawable.ic_trending_normal);
        setBottomNavigationSelected(mBinding.tvSearch, R.drawable.ic_search_selected);
        setBottomNavigationNormal(mBinding.tvSetting, R.drawable.ic_setting_normal);

        if (!is3) {
            is3 = true;
            fm.beginTransaction().add(R.id.frame_container, searchFragment, "4").addToBackStack(null).hide(searchFragment).commit();
        }
        fm.beginTransaction().hide(active).show(searchFragment).commit();
        active = searchFragment;
        if (back) {
            checkIsReady(3);
            data.add(3);
        } else {
            back = true;
        }
    }

    private void clickedHome() {
        setBottomNavigationSelected(mBinding.tvHome, R.drawable.ic_home_selected);
        setBottomNavigationNormal(mBinding.tvTrending, R.drawable.ic_trending_normal);
        setBottomNavigationNormal(mBinding.tvSearch, R.drawable.ic_search_normal);
        setBottomNavigationNormal(mBinding.tvSetting, R.drawable.ic_setting_normal);

        if (!is1) {
            is1 = true;
        }
        fm.beginTransaction().hide(active).show(homeFragment).commit();
        active = homeFragment;
        if (back) {
            checkIsReady(1);
            data.add(1);
        } else {
            back = true;
        }
    }

    public void setTitleScreen(TextView textView, String title, int icon) {
        SetGradientForText setGradientForText = new SetGradientForText();
        setGradientForText.setGradientTitleForText(textView, title, icon);
    }

    private void setBottomNavigationSelected(TextView textView, int drawable) {
        Shader textShader = new LinearGradient(0, 0, textView.getWidth(), textView.getHeight(), colorStart, colorEnd,
                Shader.TileMode.REPEAT);
        textView.getPaint().setShader(textShader);

        textView.setCompoundDrawablesWithIntrinsicBounds(0, drawable, 0, 0);
    }

    private void setBottomNavigationNormal(TextView textView, int drawable) {
        Shader textShader = new LinearGradient(0, 0, textView.getWidth(), textView.getHeight(), colorBlack, colorBlack,
                Shader.TileMode.REPEAT);
        textView.getPaint().setShader(textShader);
        textView.setCompoundDrawablesWithIntrinsicBounds(0, drawable, 0, 0);
    }

    public void showNavigation() {
        mBinding.layoutBottomNavigation.setVisibility(View.VISIBLE);
    }

    public void hideNavigation() {
        mBinding.layoutBottomNavigation.setVisibility(View.GONE);
    }

    public void pauseVideoHomeActivity() {
        videoPlayFragment.pauseVideo();
    }

    public void playVideoHomeActivity() {
        videoPlayFragment.playVideo();
    }

    public void exitVideoBackgroundHomeActivity() {
        videoPlayFragment.exitBackgroundVideo();
    }

    public void showPlayScreen(Item video) {
        checkShowMaxPlayScreen = true;
        mBinding.dragView.maximize();
        titleVideo = video.getSnippet().getTitle();
        videoPlayFragment.playVideo(video);
        subPlayFragment.reloadData(video);
    }

    public void showPlayScreenSubLayFragment(Item video) {
        titleVideo = video.getSnippet().getTitle();
        videoPlayFragment.playVideo(video);
    }

    public Item getItemFirst() {
        return subPlayFragment.getItemFirst();
    }

    public void updatePlayScreen() {
        subPlayFragment.updatePlayScreen();
    }

    public void reloadDataFromSubPlayFragment(Item video) {
        subPlayFragment.reloadData(video);
    }

    @Override
    public void onBackPressed() {
        if (checkShowMaxPlayScreen) {
            mBinding.dragView.minimize();
            checkShowMaxPlayScreen = false;
        } else {
            if (checkShowMaxPlayScreen) {
                mBinding.dragView.minimize();
                checkShowMaxPlayScreen = false;
            } else {
                if (data.size() < 1) {
                    super.onBackPressed();
                } else {
                    data.remove(data.size() - 1);
                    if (data.size() == 0) {
                        finish();
                        return;
                    } else {
                        switch (data.get(data.size() - 1)) {
                            case 1:
                                clickedHome();
                                data.remove(1);
                                break;
                            case 2:
                                clickedTrending();
                                checkIsReady(2);
                                break;
                            case 3:
                                clickedSearch();
                                checkIsReady(3);
                                break;
                            case 4:
                                clickedSetting();
                                checkIsReady(4);
                                break;
                            default:
                                finish();
                                break;
                        }
                    }
                }
            }
        }
    }

    private void checkIsReady(int i) {
        for (int j = 1; j < data.size() - 1; j++) {
            if (data.get(j) == i) {
                data.remove(j);
            }
        }
    }

}
