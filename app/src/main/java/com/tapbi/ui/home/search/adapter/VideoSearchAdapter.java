package com.tapbi.ui.home.search.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.tapbi.data.local.model.video.Item;
import com.tapbi.databinding.ItemVideoSearchBinding;
import com.tapbi.ui.home.search.SearchViewModel;
import com.tapbi.utils.ConvertCount;

import java.util.List;

public class VideoSearchAdapter extends RecyclerView.Adapter<VideoSearchAdapter.VideoHolder> {

    private LayoutInflater layoutInflater;
    public interface DataVmd {
        SearchViewModel getVmd();
    }

    private DataVmd dataVmd;

    public VideoSearchAdapter(LayoutInflater layoutInflater, DataVmd dataVmd) {
        this.dataVmd = dataVmd;
        this.layoutInflater = layoutInflater;
    }

    private List<Item> data;

    public List<Item> getData() {
        return data;
    }

    public void setData(List<Item> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public VideoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemVideoSearchBinding binding = ItemVideoSearchBinding.inflate(layoutInflater, parent, false);
        return new VideoHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class VideoHolder extends RecyclerView.ViewHolder {
        private ItemVideoSearchBinding binding;

        public VideoHolder(@NonNull ItemVideoSearchBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @SuppressLint("SetTextI18n")
        public void bind(Item item) {
            dataVmd.getVmd().getViewCountById(item,binding.viewCount,binding.tvDuration);
            ConvertCount convertCount = new ConvertCount();
            String time = convertCount.convertTime(item);
            binding.time.setText(time);
            binding.title.setText(item.getSnippet().getTitle());
            binding.channel.setText(item.getSnippet().getChannelTitle());
            Glide.with(binding.imageView).load(item.getSnippet().getThumbnails().getHigh().getUrl()).centerCrop().into(binding.imageView);
        }
    }

}
