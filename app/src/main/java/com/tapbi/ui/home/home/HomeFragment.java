package com.tapbi.ui.home.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.tapbi.R;
import com.tapbi.data.local.model.video.Item;
import com.tapbi.data.local.model.video.SOAnswersResponse;
import com.tapbi.databinding.FragmentHomeBinding;
import com.tapbi.ui.base.BaseFragment;
import com.tapbi.ui.home.HomeActivity;
import com.tapbi.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends BaseFragment<FragmentHomeBinding, HomeFragmentViewModel> implements VideoHomeFragmentAdapter.DataVmd {

    private VideoHomeFragmentAdapter adapter;
    private List<Item> videos = new ArrayList<>();

    private boolean isCheckOnCLickItem = false;

    @Override
    protected Class<HomeFragmentViewModel> getViewModelClass() {
        return HomeFragmentViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setTitleScreen(mBinding.titleHomeFragment.tvTitle
                , getResources().getString(R.string.youtube_music_splash_activity), R.drawable.icon_frame_menu);
        ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle(getString(R.string.loading));
        progressDialog.setMessage(getString(R.string.wait_while_loading));
        progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progressDialog.show();
        initializeAdapter(videos);
        mViewModel = ViewModelProviders.of(this).get(HomeFragmentViewModel.class);
        mViewModel.initHomeFragment();
        mViewModel.getNewsRepositoryHomeFragment().observe(getViewLifecycleOwner(), new Observer<SOAnswersResponse>() {
            @Override
            public void onChanged(SOAnswersResponse videoYoutube) {
                if (videoYoutube != null) {
                    List<Item> items = videoYoutube.getItems();
                    videos.addAll(items);
                    adapter.notifyDataSetChanged();
                }
            }
        });

        mBinding.rcVideoHomeFragment.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), mBinding.rcVideoHomeFragment, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (position >= 0 && !isCheckOnCLickItem
                                && !((HomeActivity) getActivity()).isCheckShowMaxPlayScreen()) {
                            isCheckOnCLickItem = true;
                            ((HomeActivity) getActivity()).showPlayScreen(videos.get(position));
                            isCheckOnCLickItem = false;
                        }
                    }
                })
        );
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        }, 4000);
    }


    private void initializeAdapter(List<Item> videos) {
        if (adapter == null) {
            adapter = new VideoHomeFragmentAdapter(getLayoutInflater(), this);
            mBinding.rcVideoHomeFragment.setAdapter(adapter);
            adapter.setData(videos);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public HomeFragmentViewModel getVmd() {
        return mViewModel;
    }

}
