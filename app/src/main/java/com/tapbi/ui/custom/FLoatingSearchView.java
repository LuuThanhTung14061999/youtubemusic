package com.tapbi.ui.custom;

import android.content.Context;
import android.util.AttributeSet;

import com.arlib.floatingsearchview.FloatingSearchView;


public class FLoatingSearchView extends FloatingSearchView {
    public FLoatingSearchView(Context context) {
        super(context);
    }

    public FLoatingSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

}
