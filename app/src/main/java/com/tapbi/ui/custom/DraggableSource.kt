package com.tapbi.ui.custom

import android.content.Context

import android.util.AttributeSet
import android.view.View

import com.tapbi.R
import com.tapbi.ui.home.HomeActivity
import com.tuanhav95.drag.DragView
import com.tuanhav95.drag.utils.inflate
import com.tuanhav95.drag.utils.reWidth
import kotlinx.android.synthetic.main.activity_home.view.*
import kotlinx.android.synthetic.main.layout_top.view.*


class DraggableSource @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : DragView(context, attrs, defStyleAttr) {
    var mWidthWhenMax = 0
    var mWidthWhenMiddle = 0
    var mWidthWhenMin = 0
    init {
        getFrameFirst().addView(inflate(R.layout.layout_top))
        getFrameSecond().addView(inflate(R.layout.layout_bottom))
    }
    override fun initFrame() {
        mWidthWhenMax = width
        mWidthWhenMiddle = (width - mPercentWhenMiddle * mMarginEdgeWhenMin).toInt()
        mWidthWhenMin = mHeightWhenMinDefault * 16 / 9
        super.initFrame()
    }
    override fun refreshFrameFirst() {
        super.refreshFrameFirst()
        val width = if (mCurrentPercent < mPercentWhenMiddle) {
            (context as HomeActivity).hideNavigation()
            (context as HomeActivity).isCheckShowMaxPlayScreen = true
            (mWidthWhenMax - (mWidthWhenMax - mWidthWhenMiddle) * mCurrentPercent)

        } else {
            (context as HomeActivity).showNavigation()
            (context as HomeActivity).isCheckShowMaxPlayScreen = false
            (mWidthWhenMiddle - (mWidthWhenMiddle - mWidthWhenMin) * (mCurrentPercent - mPercentWhenMiddle) / (1 - mPercentWhenMiddle))
        }
        frameTop.reWidth(width.toInt())
        ivClose.setOnClickListener(OnClickListener {
            dragView.close()
            (context as HomeActivity).exitVideoBackgroundHomeActivity()
        })
        ivPause.setOnClickListener(OnClickListener {
            (context as HomeActivity).pauseVideoHomeActivity()
            ivPause.visibility = View.GONE
            ivPlay.visibility = View.VISIBLE
        })
        ivPlay.setOnClickListener(OnClickListener {
            (context as HomeActivity).playVideoHomeActivity()
            ivPause.visibility = View.VISIBLE
            ivPlay.visibility = View.GONE
        })
        tvTitle.text = (context as HomeActivity).titleVideo
        tvChannel.text = (context as HomeActivity).channelVideo
    }
}