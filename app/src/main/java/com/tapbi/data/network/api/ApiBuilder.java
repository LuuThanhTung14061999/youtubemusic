package com.tapbi.data.network.api;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
public abstract class ApiBuilder {
    private static YoutubeApi api;

    public static YoutubeApi getInstance() {
        if (api == null) {
            api = new Retrofit.Builder()
                    .baseUrl("https://www.googleapis.com/youtube/v3/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(YoutubeApi.class);
        }
        return api;
    }
}
