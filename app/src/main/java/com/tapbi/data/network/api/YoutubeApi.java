package com.tapbi.data.network.api;

import com.tapbi.data.local.model.channel.ChannelYoutube;
import com.tapbi.data.local.model.comment.CommentYoutube;
import com.tapbi.data.local.model.video.SOAnswersResponse;
import com.tapbi.data.local.model.video.VideoYoutube;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface YoutubeApi {
    @GET("search/")
    Call<VideoYoutube> getAnswers(@Query("key") String key,
                                       @Query("part") String part_snippet,
                                       @Query("q") String search,
                                       @Query("type") String type,
                                       @Query("maxResults") int maxResult
    );

    @GET("search/")
    Call<SOAnswersResponse> getAnswersSearch(@Query("key") String key,
                                             @Query("part") String part_snippet,
                                             @Query("q") String search,
                                             @Query("type") String type,
                                             @Query("maxResults") int maxResult
    );

    @GET("videos?part=snippet&part=contentDetails&part=statistics")
    Call<VideoYoutube> getVideo(@Query("id") String videoId,
                                @Query("key") String key

    );

    @GET("videos?part=snippet,contentDetails,statistics&chart=mostPopular")
    Call<VideoYoutube> getVideoTrendingYoutube(@Query("maxResults") int maxResults,
                                       @Query("key") String key,
                                       @Query("regionCode") String regionCode
    );


    @GET("channels?part=snippet&part=statistics")
    Call<ChannelYoutube> getChannelYoutube(@Query("id") String channelId,
                                           @Query("key") String key
    );


    @GET("commentThreads?part=snippet")
    Call<CommentYoutube> getCommentYoutube(@Query("maxResults") int maxResults,
                                           @Query("videoId") String videoId,
                                           @Query("key") String key);
}
