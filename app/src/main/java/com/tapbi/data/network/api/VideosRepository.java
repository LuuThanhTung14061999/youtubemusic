package com.tapbi.data.network.api;

import android.annotation.SuppressLint;
import android.os.Build;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.lifecycle.MutableLiveData;

import com.bumptech.glide.Glide;
import com.tapbi.data.local.model.channel.ChannelYoutube;
import com.tapbi.data.local.model.comment.CommentYoutube;
import com.tapbi.data.local.model.video.SOAnswersResponse;
import com.tapbi.data.local.model.video.VideoYoutube;
import com.tapbi.utils.ConvertCount;

import java.time.Duration;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideosRepository {
    private static VideosRepository videosRepository;

    public static VideosRepository getInstance() {
        if (videosRepository == null) {
            videosRepository = new VideosRepository();
        }
        return videosRepository;
    }

    private YoutubeApi youtubeAPI;

    public VideosRepository() {
        youtubeAPI = ApiBuilder.getInstance();
    }

    public MutableLiveData<SOAnswersResponse> getVideosSearch(String key, String part_snippet, String q, String type, int maxResult) {
        MutableLiveData<SOAnswersResponse> videoData = new MutableLiveData<>();
        youtubeAPI.getAnswersSearch(key, part_snippet, q, type, maxResult).enqueue(new Callback<SOAnswersResponse>() {
            @Override
            public void onResponse(Call<SOAnswersResponse> call,
                                   Response<SOAnswersResponse> response) {
                if (response.isSuccessful()) {
                    videoData.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SOAnswersResponse> call, Throwable t) {
                videoData.postValue(null);
            }
        });
        return videoData;
    }

    public void getVideo(String videoId, String key, TextView textView, TextView textView1) {
        MutableLiveData<VideoYoutube> videoData = new MutableLiveData<>();
        youtubeAPI.getVideo(videoId, key).enqueue(new Callback<VideoYoutube>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<VideoYoutube> call,
                                   Response<VideoYoutube> response) {
                if (response.isSuccessful()) {
                    ConvertCount convertCount = new ConvertCount();
                    if(textView.getText().toString().equals("")){
                        textView.setText(convertCount.convertViewCount(response.body()) + " - ");
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O &&textView1.getText().toString().equals("")) {
                        textView1.setText(convertCount.convertDuration(Duration.parse(response.body().getItems().get(0).getContentDetails().getDuration()).getSeconds()) + "");
                    }
                    videoData.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<VideoYoutube> call, Throwable t) {
                videoData.postValue(null);
            }
        });

    }

    public void getVideo(String videoId, String key, TextView textView1
            , TextView textView2, TextView textView3, TextView textView4, CardView cardView, TextView textView5) {
        MutableLiveData<VideoYoutube> videoData = new MutableLiveData<>();
        youtubeAPI.getVideo(videoId, key).enqueue(new Callback<VideoYoutube>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<VideoYoutube> call,
                                   Response<VideoYoutube> response) {
                if (response.isSuccessful()) {
                    ConvertCount convertCount = new ConvertCount();
                    if(textView1.getText().toString().equals("")){
                        textView1.setText(convertCount.convertViewCount(response.body()) + " - ");
                    }
                    if(textView2.getText().toString().equals(""))
                    convertCount.convertLikeCount(response.body(), textView2);
                    if(textView3.getText().toString().equals(""))
                        convertCount.convertDislikeCount(response.body(), textView3);
                    if(textView4.getText().toString().equals(""))
                        convertCount.convertCommentCount(response.body(),textView4,cardView,textView5);
                    videoData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<VideoYoutube> call, Throwable t) {
                videoData.setValue(null);
            }
        });
    }

    public MutableLiveData<ChannelYoutube> getChannel(String channelId, String key, ImageView imageView, TextView textView) {
        MutableLiveData<ChannelYoutube> channelData = new MutableLiveData<>();
        youtubeAPI.getChannelYoutube(channelId, key).enqueue(new Callback<ChannelYoutube>() {
            @Override
            public void onResponse(Call<ChannelYoutube> call,
                                   Response<ChannelYoutube> response) {
                if (response.isSuccessful()) {
                    channelData.setValue(response.body());
                    ConvertCount convertCount = new ConvertCount();
                    if (response.body() != null) {
                        convertCount.convertViewCountChannel(textView, response.body());
                    }
                    Glide.with(imageView).load(response.body().getItemChannels().get(0).getSnippet().getThumbnails().getHigh().getUrl()).centerCrop().into(imageView);
                }
            }

            @Override
            public void onFailure(Call<ChannelYoutube> call, Throwable t) {
                channelData.setValue(null);
            }
        });
        return channelData;
    }

    public MutableLiveData<ChannelYoutube> getChannel(String channelId, String key, ImageView imageView) {
        MutableLiveData<ChannelYoutube> channelData = new MutableLiveData<>();
        youtubeAPI.getChannelYoutube(channelId, key).enqueue(new Callback<ChannelYoutube>() {
            @Override
            public void onResponse(Call<ChannelYoutube> call,
                                   Response<ChannelYoutube> response) {
                if (response.isSuccessful()) {
                    if(imageView.getDrawable()==null){
                        Glide.with(imageView).load(response.body().getItemChannels().get(0).getSnippet().getThumbnails().getHigh().getUrl()).centerCrop().into(imageView);
                    }
                    channelData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ChannelYoutube> call, Throwable t) {
                channelData.setValue(null);
            }
        });
        return channelData;
    }

    public MutableLiveData<CommentYoutube> getComment(int maxResult, String videoId, String key, ImageView imageView, TextView textView) {
        MutableLiveData<CommentYoutube> channelData = new MutableLiveData<>();
        youtubeAPI.getCommentYoutube(maxResult, videoId, key).enqueue(new Callback<CommentYoutube>() {
            @Override
            public void onResponse(Call<CommentYoutube> call,
                                   Response<CommentYoutube> response) {
                if (response.isSuccessful()) {
                    channelData.setValue(response.body());
                    textView.setText(response.body().getItemComments().get(0)
                            .getSnippet().getTopLevelComment().getSnippet().getTextOriginal());
                    Glide.with(imageView)
                            .load(response.body().getItemComments().get(0)
                                    .getSnippet().getTopLevelComment().getSnippet().getAuthorProfileImageUrl())
                            .into(imageView);
                }
            }

            @Override
            public void onFailure(Call<CommentYoutube> call, Throwable t) {
                channelData.setValue(null);
            }
        });
        return channelData;
    }

}
