package com.tapbi.data.local.model.comment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemComment {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("snippet")
    @Expose
    private SnippetComment snippet;

    public ItemComment(String id, SnippetComment snippet) {
        this.id = id;
        this.snippet = snippet;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SnippetComment getSnippet() {
        return snippet;
    }

    public void setSnippet(SnippetComment snippet) {
        this.snippet = snippet;
    }
}
