package com.tapbi.data.local.model.channel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemChannel {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("snippet")
    @Expose
    private SnippetChannel snippet;
    @SerializedName("statistics")
    @Expose
    private StatisticChannel statistics;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SnippetChannel getSnippet() {
        return snippet;
    }

    public void setSnippet(SnippetChannel snippet) {
        this.snippet = snippet;
    }

    public StatisticChannel getStatistics() {
        return statistics;
    }

    public void setStatistics(StatisticChannel statistics) {
        this.statistics = statistics;
    }
}