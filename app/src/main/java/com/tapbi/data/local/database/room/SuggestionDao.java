package com.tapbi.data.local.database.room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.tapbi.ui.home.search.model.Suggestion;

import java.util.List;

@Dao
public interface SuggestionDao {
    @Insert
     void insertSuggestion(Suggestion... Suggestions);

    @Query("select * from Suggestion")
    List<Suggestion> getAll();

    @Query("select mName from Suggestion where mName=:name ")
    String getSuggestion(String name);

    @Query("select count(*) from Suggestion")
    int countAll();
}
