package com.tapbi.data.local.model.comment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommentYoutube {

    @SerializedName("items")
    @Expose
    private List<ItemComment> itemComments;

    public CommentYoutube(List<ItemComment> itemComments) {
        this.itemComments = itemComments;
    }

    public List<ItemComment> getItemComments() {
        return itemComments;
    }

    public void setItemComments(List<ItemComment> itemComments) {
        this.itemComments = itemComments;
    }
}
