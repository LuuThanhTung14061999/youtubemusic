package com.tapbi.data.local.model.channel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChannelYoutube {
    @SerializedName("items")
    @Expose
    private List<ItemChannel> itemChannels;

    public List<ItemChannel> getItemChannels() {
        return itemChannels;
    }

    public void setItemChannels(List<ItemChannel> itemChannels) {
        this.itemChannels = itemChannels;
    }

}
