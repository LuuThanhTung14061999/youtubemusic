package com.tapbi.data.local.model.comment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommentResource {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("snippet")
    @Expose
    private SnippetCommentResource snippet;

    public CommentResource(String id, SnippetCommentResource snippet) {
        this.id = id;
        this.snippet = snippet;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SnippetCommentResource getSnippet() {
        return snippet;
    }

    public void setSnippet(SnippetCommentResource snippet) {
        this.snippet = snippet;
    }
}
