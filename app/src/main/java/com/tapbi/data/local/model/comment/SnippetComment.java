package com.tapbi.data.local.model.comment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SnippetComment {

    @SerializedName("channelId")
    @Expose
    private String channelId;

    @SerializedName("videoId")
    @Expose
    private String videoId;

    @SerializedName("topLevelComment")
    @Expose
    private CommentResource topLevelComment;

    @SerializedName("canReply")
    @Expose
    private String canReply;

    @SerializedName("totalReplyCount")
    @Expose
    private String totalReplyCount;

    public SnippetComment(String channelId, String videoId, CommentResource topLevelComment, String canReply, String totalReplyCount) {
        this.channelId = channelId;
        this.videoId = videoId;
        this.topLevelComment = topLevelComment;
        this.canReply = canReply;
        this.totalReplyCount = totalReplyCount;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public CommentResource getTopLevelComment() {
        return topLevelComment;
    }

    public void setTopLevelComment(CommentResource topLevelComment) {
        this.topLevelComment = topLevelComment;
    }

    public String getCanReply() {
        return canReply;
    }

    public void setCanReply(String canReply) {
        this.canReply = canReply;
    }

    public String getTotalReplyCount() {
        return totalReplyCount;
    }

    public void setTotalReplyCount(String totalReplyCount) {
        this.totalReplyCount = totalReplyCount;
    }
}
