package com.tapbi.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.tapbi.R;
import com.tapbi.service.MyService;
import com.tapbi.ui.home.HomeActivity;
import com.tapbi.utils.App;

import static android.app.Notification.DEFAULT_ALL;

public class AlarmReceiver extends BroadcastReceiver {

    private NotificationManager mNotificationManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(App.getContext(), "notify_001");
        Intent ii = new Intent(App.getContext(), HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(App.getContext(), 0, ii, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(App.getContext().getString(R.string.title_notification));
        bigText.setBigContentTitle("");
        bigText.setSummaryText("");
        mBuilder.setDefaults(DEFAULT_ALL);
        mBuilder.setWhen(System.currentTimeMillis());
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.drawable.icon_frame_menu);
        mBuilder.setContentTitle(App.getContext().getString(R.string.youtube_music_splash_activity));
        mBuilder.setContentText(App.getContext().getString(R.string.title_notification));
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setStyle(bigText).setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
                .setContentInfo("Info");
        mNotificationManager =
                (NotificationManager) App.getContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "notify_001";
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH);
            mNotificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId(channelId);
        }
        mNotificationManager.notify(1, mBuilder.build());
        // TODO Auto-generated method stub
        Intent service1 = new Intent(context, MyService.class);
        context.startService(service1);
    }

}