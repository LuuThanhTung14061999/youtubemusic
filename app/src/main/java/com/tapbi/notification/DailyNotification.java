package com.tapbi.notification;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ListenableWorker;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.tapbi.R;
import com.tapbi.ui.home.HomeActivity;
import com.tapbi.utils.App;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static android.app.Notification.DEFAULT_ALL;
import static com.tapbi.common.Constant.REMINDER_WORK_NAME;

public class DailyNotification extends Worker {

    public DailyNotification(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    private NotificationManager mNotificationManager;

    private static void runAt() {
        WorkManager workManager = WorkManager.getInstance();
        LocalTime alarmTime = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            alarmTime = LocalTime.of(7, 1,1);
            LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES);
            LocalTime nowTime = now.toLocalTime();
            if (nowTime == alarmTime || nowTime.isAfter(alarmTime)) {
                now = now.plusDays(1);
            }
            now = now.withHour(alarmTime.getHour()).withMinute(alarmTime.getMinute());
            Duration duration = Duration.between(LocalDateTime.now(), now);

            OneTimeWorkRequest workRequestnew = new OneTimeWorkRequest.Builder(DailyNotification.class)
                    .setInitialDelay(duration.getSeconds(), TimeUnit.SECONDS)
                    .build();
            workManager.enqueueUniqueWork(REMINDER_WORK_NAME, ExistingWorkPolicy.REPLACE, workRequestnew);
        }
    }

    @NonNull
    @Override
    public ListenableWorker.Result doWork() {
        boolean isScheduleNext = true;
        try {
            createNotification();
            Result.success();
        } catch (Exception e) {
            if (getRunAttemptCount() > 3) {
                return Result.success();
            }

        }finally {
            if (isScheduleNext) {
                runAt();
            }
        }
        return null;
    }

    private void createNotification() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext(), "notify_001");
        Intent ii = new Intent(getApplicationContext(), HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, ii, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(App.getContext().getString(R.string.title_notification));
        bigText.setBigContentTitle("");
        bigText.setSummaryText("");
        mBuilder.setDefaults(DEFAULT_ALL);
        mBuilder.setWhen(System.currentTimeMillis());
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.drawable.icon_frame_menu);
        mBuilder.setContentTitle(App.getContext().getString(R.string.youtube_music_splash_activity));
        mBuilder.setContentText(App.getContext().getString(R.string.title_notification));
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setStyle(bigText).setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
                .setContentInfo("Info");
        mNotificationManager =
                (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "notify_001";
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH);
            mNotificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId(channelId);
        }
        mNotificationManager.notify(1, mBuilder.build());
    }
}
