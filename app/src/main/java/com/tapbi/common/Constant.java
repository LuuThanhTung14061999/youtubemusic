package com.tapbi.common;

import androidx.core.content.ContextCompat;

import com.tapbi.R;

import static com.tapbi.utils.App.getContext;

public class Constant {
    public static final String API_KEY = "AIzaSyBo8ywQlClx5g5G0HQkCRU_1RGHixpkUq4";
    public static final String PART_SNIPPET = "snippet";

    public static final String DATABASE_NAME = "Room-database_";
    public static final String REMINDER_WORK_NAME = "reminder";

    public static final int colorStart = ContextCompat.getColor(getContext(), R.color.text_red_gran_2);
    public static final int colorEnd = ContextCompat.getColor(getContext(), R.color.text_red_gran_1);
    public static final int colorBlack = ContextCompat.getColor(getContext(), R.color.black);

    //key thay thế: AIzaSyBM6qwxDPhl-cNayqIv5LK6UZxy2JaAgYo
}
